#ifndef MARKWINDOW_H
#define MARKWINDOW_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class MarkWindow : public QDialog
{
    Q_OBJECT
public:
    enum MarkType { TextMark, ImageMark };
    explicit MarkWindow(QWidget *parent = nullptr);
    ~MarkWindow();

    void setStayOnTop(bool top);
    void setMark(QString text, QColor color, QString font, int font_size,
                     int hspace, int vspace, float alpha, float angle);
    void setMark(QString image, float alpha, int scale);

private:
    Ui::Dialog *ui;

};

#endif // MARKWINDOW_H

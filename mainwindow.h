#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_mainwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_toolButton_clicked();
    void on_cbScale_activated(int index);
    void on_pbRefresh_clicked();

    void on_pbAdd_clicked();

    void on_pbShow_clicked(bool checked);

    void on_pbRemove_clicked();

    void on_cbMode_currentIndexChanged(int index);

    void on_cbStyle_currentIndexChanged(int index);

    void on_pbApply_clicked();

private:
    Ui::MainWindow *ui;
protected:
    void closeEvent(QCloseEvent *event);
};
#endif // MAINWINDOW_H

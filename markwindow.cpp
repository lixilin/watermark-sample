#include "markwindow.h"
#include <cmath>
#include "ui_dialog.h"
#include <QDebug>
#include <X11/extensions/shape.h>
#include <QPainter>
#include <QBitmap>

#include <QApplication>
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QX11Info>
#else
#include <QtX11Extras/QX11Info>
#endif

MarkWindow::MarkWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    // setAttribute(Qt::WA_TransparentForMouseEvents, true);

    // X11BypassWindowManagerHint可以使窗口脱离窗口管理器约束，水印可以移动到窗口外，但堆叠关系不能自动处理
    //this->setWindowFlags(Qt::X11BypassWindowManagerHint);
    //this->setWindowFlags(Qt::FramelessWindowHint | Qt::SplashScreen | Qt::X11BypassWindowManagerHint);

    this->setWindowFlags(Qt::FramelessWindowHint | Qt::SplashScreen);

    // this->setWindowFlags(Qt::Dialog);

    XShapeCombineRectangles(QX11Info::display(), winId(), ShapeInput,0,0, NULL, 0, ShapeSet, YXBanded);

    // this->setAttribute(Qt::WA_TranslucentBackground);
    this->setAutoFillBackground(true);
    // setWindowOpacity(0);
}

void MarkWindow::setStayOnTop(bool top)
{
    bool vis = this->isVisible();
    if (vis == true)
        this->hide();
    Qt::WindowFlags flags = this->windowFlags();
    if (top)
        flags |= Qt::WindowStaysOnTopHint;
    else
        flags &= ~Qt::WindowStaysOnTopHint;
    this->setWindowFlags(flags);
    // this->show();
    qDebug() << "Refresh";
    if (vis == true)
        this->show();
}

void MarkWindow::setMark(QString text, QColor color, QString font_name, int font_size,
                 int hspace, int vspace, float alpha, float angle)
{
    this->ui->lImage->hide();

    QFont font(font_name, font_size, QFont::Bold, false);
    QFontMetrics fmt(font);

    QPixmap pix(this->width(), this->height());
    pix.fill(Qt::transparent);

    int width = sqrt(pow(this->width(), 2) + pow(this->height(), 2));
    // int width = sqrt(pow(4096, 2) + pow(4096, 2));

    int text_width = fmt.width(text, text.length());

    QPainter painter(&pix);

    painter.translate(QPointF(pix.width() / 2, pix.height() / 2));
    painter.rotate(angle);

    painter.setFont(font);
    painter.setPen(QColor(0xFF, 0xFF, 0xFF));

    int i = 0;
    for (int x = - width / 2; x < width / 2; x += text_width + vspace) {
        int shift = (i % 2) ? (hspace + fmt.height()) / 2 : 0;
        for (int y = - width / 2; y < width / 2; y += fmt.height() + hspace) {
            QRect rect(x, y + shift, text_width + 10, fmt.height());
            painter.drawText((const QRectF)(rect), text);
        }
        i += 1;
    }

    this->clearMask();
    this->setMask(pix.mask());

    QPalette pal(this->palette());

    pal.setColor(QPalette::Background, color);
    this->setPalette(pal);

    setWindowOpacity(alpha);
}

void MarkWindow::setMark(QString image, float alpha, int scale)
{
    this->ui->lImage->show();

    QPixmap pix(image);
    QPixmap dest;
    switch (scale) {
        case 0: dest = pix.scaled(this->size() ,Qt::KeepAspectRatio); break;
        case 1: dest = pix.scaled(pix.width() * 0.5, pix.height() * 0.5 ,Qt::KeepAspectRatio); break;
        case 2: dest = pix.scaled(pix.width() * 1.0, pix.height() * 1.0 ,Qt::KeepAspectRatio); break;
        case 3: dest = pix.scaled(pix.width() * 2.0, pix.height() * 2.0 ,Qt::KeepAspectRatio); break;
    }

    ui->lImage->move(0, 0);
    ui->lImage->resize(this->width(), this->height());

    QPixmap mask(this->size());

    mask.fill(Qt::transparent);
    QPainter painter(&mask);

    painter.setPen(QColor(0xFF, 0xFF, 0xFF));
    painter.setBrush(QColor(0, 0, 0));

    QRect rect((mask.width() - dest.width()) / 2,
               (mask.height() - dest.height()) / 2,
               dest.width(), dest.height());
    qDebug() << rect;

    painter.drawRect(rect);

    this->clearMask();
    this->setMask(mask.mask());

    QPalette pal(this->palette());
    pal.setColor(QPalette::Background, Qt::white);
    this->setPalette(pal);

    ui->lImage->setPixmap(dest);
    setWindowOpacity(alpha);
}

MarkWindow::~MarkWindow()
{
    delete ui;
}

#include "xhelper.h"
#include <cstdlib>
#include <cstring>
#include <QDebug>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include <string>

#define MAX_PROPERTY_VALUE_LEN 4096

static void __enumWindow(Display *disp, Window parent, Atom *const pidAtom, int depth, QMap<WId, XHelper::WindowInfo> &snapshot)
{
    Window rootWnd, parentWnd, *childrenWnd;
    unsigned  count;

    if(0 != XQueryTree(disp, parent, &rootWnd, &parentWnd, &childrenWnd, &count)) {
        for(size_t i = 0; i < count; i++) {
            Atom type;
            int format;
            unsigned long nItems, bytesAfter;
            unsigned char *propPID = NULL;

            if(Success == XGetWindowProperty( disp, childrenWnd[i], *pidAtom, 0, 1, False, XA_CARDINAL,
                        &type, &format, &nItems, &bytesAfter, &propPID)) {
                if (propPID) {
                    snapshot.insert(childrenWnd[i], XHelper::WindowInfo(childrenWnd[i], parent, *(pid_t *)propPID, depth));
                }
            }

            __enumWindow(disp, childrenWnd[i], pidAtom, depth + 1, snapshot);
        }
    }
}

int XHelper::CreateWindowSnapshot(Display *dpy, QMap<WId, WindowInfo> &snapshot)
{
    Atom pidAtom = XInternAtom(dpy, "_NET_WM_PID", True);
    snapshot.clear();
    __enumWindow(dpy, DefaultRootWindow(dpy), &pidAtom, 0, snapshot);
    return 0;
}

pid_t XHelper::FindWindowPid(Display *dpy, WId w)
{
    char *pid_prop = XHelper::GetWindowProperty(dpy, w, XA_CARDINAL, "_NET_WM_PID", NULL);
    pid_t pid = 0;
    if (pid_prop) {
        pid = *(pid_t *)pid_prop;
        free(pid_prop);
    } else {
        // auto top = XHelper::GetWindowTopAncestor(dpy, w);
        Atom pidAtom = XInternAtom(dpy, "_NET_WM_PID", True);
        QMap<WId, WindowInfo> snapshot;
        snapshot.clear();
        __enumWindow(dpy, w, &pidAtom, 0, snapshot);
        for (auto it : snapshot) {
            if (it.m_pid != 0) {
                pid = it.m_pid;
                break;
            }
        }
    }
    return pid;
}

char *XHelper::GetWindowProperty(Display *dpy, WId win,
        Atom xa_prop_type, const char *prop_name, size_t *size)
{
    Atom xa_prop_name;
    Atom xa_ret_type;
    int ret_format;
    unsigned long ret_nitems, ret_bytes_after, tmp_size;
    unsigned char *ret_prop;
    char *ret;

    xa_prop_name = XInternAtom(dpy, prop_name, False);

    /* MAX_PROPERTY_VALUE_LEN / 4 explanation (XGetWindowProperty manpage):
     *
     * long_length = Specifies the length in 32-bit multiples of the
     *               data to be retrieved.
     */
    if (XGetWindowProperty(dpy, win, xa_prop_name, 0, MAX_PROPERTY_VALUE_LEN / 4, False, xa_prop_type, &xa_ret_type, &ret_format,
            &ret_nitems, &ret_bytes_after, &ret_prop) != Success) {
        return NULL;
    }
    if (xa_ret_type != xa_prop_type) {
        XFree(ret_prop);
        return NULL;
    }

    tmp_size = (ret_format / (32 / sizeof(long))) * ret_nitems;
    ret = (char *)malloc(tmp_size + 1);
    memcpy(ret, ret_prop, tmp_size);
    ret[tmp_size] = '\0';

    if (size) {
        *size = tmp_size;
    }

    XFree(ret_prop);
    return ret;
}

int XHelper::GetWindowRect(Display *dpy, WId w, QRect &rect)
{
    int x, y;
    unsigned int wwidth, wheight, bw, depth;
    Window junkroot;

    if (True != XGetGeometry (dpy, w, &junkroot, &x, &y,
                      &wwidth, &wheight, &bw, &depth))
        return -1;
    rect = QRect(x, y, wwidth, wheight);
    return 0;
}

WId XHelper::GetWindowParent(Display *dpy, WId w)
{
    Window root_wnd, parent_wnd, *children;
    unsigned int nchildren;
    int ret = XQueryTree(dpy, w, &root_wnd, &parent_wnd, &children, &nchildren);
    if (ret == False) {
        return 0;
    }
    XFree(children);
    return parent_wnd;
}

WId XHelper::GetWindowTopAncestor(Display *dpy, WId w)
{
    auto root = DefaultRootWindow(dpy);
    auto parent = GetWindowParent(dpy, w);
    while (parent != root) {
        w = parent;
        parent = GetWindowParent(dpy, w);
        if (parent == 0)
            return 0;
    }
    return w;
}

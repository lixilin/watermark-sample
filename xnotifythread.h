#ifndef XNOTIFYTHREAD_H
#define XNOTIFYTHREAD_H


#include <QObject>
#include <QThread>
#include <QSet>
#include <QMap>
#include <QRect>
#include <QMutex>
#include <QWidget>


typedef struct _XDisplay Display;

class XNotifyThread : public QThread
{
    Q_OBJECT
public:
    ~XNotifyThread();
    explicit XNotifyThread(QObject *parent = nullptr);
    void attachPid(pid_t pid);
    void detachPid(pid_t pid);
private:
    struct WindowInfo {
        WindowInfo(pid_t pid, bool viewable)
            : m_pid(pid), m_viewable(viewable) { }
        pid_t m_pid;
        bool m_viewable;
    };

    QMap<pid_t, QSet<WId>> m_pidToWindowSet;
    QMap<WId, WindowInfo> m_windowToPid;

    bool addWindowIfNotExist(WId w, const WindowInfo &wi) {
        auto it = m_windowToPid.find(w);
        if (it == m_windowToPid.end()) {
            m_pidToWindowSet[wi.m_pid].insert(w);
            m_windowToPid.insert(w, wi);
            return true;
        }
        return false;
    }

    bool removeWindow(WId w) {
        auto it = m_windowToPid.find(w);
        if (it != m_windowToPid.end()) {
            m_pidToWindowSet[it->m_pid].remove(w);
            m_windowToPid.erase(it);
            return true;
        }
        return false;
    }

    pid_t findWindowByPid(WId w);
    void ForceSyncAllWindow();

    QMutex m_lock;
    Display *m_dpy;
protected:
    void run();
signals:
    void windowFound(WId w, pid_t pid);
    void windowLost(WId w);
    // void windowResized(WId w, QRect rect, QRect clientRect);
    void windowFocusIn(WId w);
    void windowFocusOut(WId w);
    void windowResized(WId w, QSize size);

};

#endif // XNOTIFYTHREAD_H

#ifndef MARK_H
#define MARK_H

#include <QWidget>

namespace Ui {
class Mark;
}

class Mark : public QWidget
{
    Q_OBJECT
public:
    enum MarkType { TextMark, ImageMark };
    explicit Mark(QWidget *parent = nullptr);
    ~Mark();

    void setStayOnTop(bool top);
    void setMark(QColor color, QString font, int font_size,
                     int hspace, int vspace, float alpha, float angle);
    void setMark(QString image, float alpha, int scale);

    void setText(QString text);

private:
    QString m_text = "NO TEXT";
    Ui::Mark *ui;

};

#endif // MARK_H

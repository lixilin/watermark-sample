#ifndef WATERMARKMANAGER_H
#define WATERMARKMANAGER_H

#include "xnotifythread.h"
#include "mark.h"

#include <QColor>
#include <QMap>

class WatermarkManager: QObject
{
    Q_OBJECT
public:
    enum Mode { FullScreen, ProcessWindow, UnsetMode };
    enum Style { Text, Image, UnsetStyle };

    static WatermarkManager* get_instance(){
        if (!m_instance){
              m_instance = new WatermarkManager;
        }
        return m_instance;
    }

    void setMark(QColor color, QString font, int font_size, int hspace, int vspace, float alpha, float angle)
    {
        m_textMark.font = font;
        m_textMark.color = color;
        m_textMark.fontSize = font_size;
        m_textMark.hspace = hspace;
        m_textMark.vspace = vspace;
        m_textMark.alpha = alpha;
        m_textMark.angle = angle;
        m_textMark.havaData = true;
        m_style = Text;
        update();
    }

    void setMark(QString image, float alpha, int scale)
    {
        m_imageMark.image = image;
        m_imageMark.alpha = alpha;
        m_imageMark.scale = scale;
        m_imageMark.havaData = true;
        m_style = Image;
        update();
    }

    void setFullScreenModeText(QString text)
    {
        m_markText = text;
    }

    void setMode(Mode mode);
    void bindPID(pid_t pid, QString text);
    void unbindPID(pid_t pid);



    void show();
    void hide();
private:
    WatermarkManager();
    WatermarkManager(WatermarkManager&)=delete;
    WatermarkManager& operator=(const WatermarkManager&)=delete;

    void updateAllMark();
    void updateVisibility();

    void update();

    struct {
        bool havaData = false;
        // QString text;
        QColor color;
        QString font;
        int fontSize;
        int hspace;
        int vspace;
        float alpha;
        float angle;
    } m_textMark;

    struct {
        bool havaData = false;
        QString image;
        float alpha;
        int scale;
    } m_imageMark;

    struct MarkInfo {
        Mark m_mark;
        pid_t m_pid;
    };

    XNotifyThread m_notifyThread;
    Style m_style = UnsetStyle;
    Mode m_mode = UnsetMode;
    QString m_markText = "FULLSCREEN WATERMARK";
    static WatermarkManager *m_instance;
    Mark m_markWindow;
    QMap<uint32_t, MarkInfo*> m_windowToMarkInfo;
    QMap<pid_t, QString> m_pidToText;

    bool m_visible = false;
private slots:

    void desktopResized(int n);
    void screenCountChanged(int);
    void primaryScreenChanged();

    void windowFound(WId w, pid_t pid);
    void windowLost(WId w);
    void windowResized(WId w, QSize size);
    void windowFocusIn(WId w);
    void windowFocusOut(WId w);
};

#endif // WATERMARKMANAGER_H

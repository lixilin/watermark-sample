#ifndef XHELPER_H
#define XHELPER_H

#include <QWidget>
#include <QMap>

typedef struct _XDisplay Display;

#  ifndef _XTYPEDEF_ATOM
#    define _XTYPEDEF_ATOM
typedef unsigned long Atom;
#  endif

class XHelper
{
public:
    struct WindowInfo {
        WindowInfo(WId wid, WId parent, pid_t pid, int depth)
            : m_wid(wid), m_parentWid(parent), m_pid(pid), m_depth(depth) { }
        WId m_wid;
        WId m_parentWid;
        pid_t m_pid;
        int m_depth;
    };
    static int CreateWindowSnapshot(Display *dpy, QMap<WId, WindowInfo> &snapshot);
    static char *GetWindowProperty (Display *dpy, WId win, Atom xa_prop_type,
                                   const char *prop_name, size_t *size);
    static int GetWindowRect(Display *display, WId w, QRect &rect);
    static WId GetWindowParent(Display *display, WId w);
    static WId GetWindowTopAncestor(Display *display, WId w);
    static pid_t FindWindowPid(Display *dpy, WId w);
private:
    explicit XHelper() { }
    XHelper(XHelper&) = delete;
    XHelper& operator = (const XHelper&) = delete;
};

#endif // XHELPER_H

#include <QDebug>
#include "xnotifythread.h"
#include "xhelper.h"
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <cstdlib>

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QX11Info>
#else
#include <QtX11Extras/QX11Info>
#endif

XNotifyThread::XNotifyThread(QObject *parent) : QThread(parent)
{
    m_dpy = XOpenDisplay(0);
    if (!m_dpy)
        throw std::bad_alloc();
}

XNotifyThread::~XNotifyThread()
{
    XCloseDisplay(m_dpy);
}

pid_t XNotifyThread::findWindowByPid(WId w)
{
    pid_t ret = 0;
    m_lock.lock();
    auto it = this->m_windowToPid.find(w);
    if (it != this->m_windowToPid.end())
        ret = it.value().m_pid;
    m_lock.unlock();
    return ret;
}

void XNotifyThread::ForceSyncAllWindow()
{

    Display *dpy = XOpenDisplay(0);
    QMap<WId, XHelper::WindowInfo> snapshot;
    XHelper::CreateWindowSnapshot(dpy, snapshot);
    m_lock.lock();
    for (auto item : snapshot) {
        auto it = m_pidToWindowSet.find(item.m_pid);
        if (it != m_pidToWindowSet.end()) {
            qDebug() << "m1";
            auto topWindow = XHelper::GetWindowTopAncestor(dpy, item.m_wid);
            if (topWindow == 0)
                continue;
            qDebug() << "m2";
            XWindowAttributes attr;
            if (!XGetWindowAttributes(dpy, topWindow, &attr))
                continue;
            qDebug() << "m3";
            if (attr.map_state == IsUnmapped)
                continue;
            qDebug() << "m4";
            auto it1 = m_windowToPid.find(topWindow);
            if (it1 == m_windowToPid.end()) {
                qDebug() << "m5";
                WindowInfo wi(item.m_pid, false);
                m_windowToPid.insert(topWindow, wi);
                it.value().insert(topWindow);
                emit windowFound(topWindow, item.m_pid);
                QRect rect;
                if (XHelper::GetWindowRect(dpy, item.m_wid, rect) != -1) {
                    emit windowResized(topWindow, QSize(rect.width(), rect.height()));
                }
            }
        }
    }
    XCloseDisplay(dpy);
    m_lock.unlock();
}

void XNotifyThread::run()
{
    auto rootwin = DefaultRootWindow(m_dpy);

    ForceSyncAllWindow();

    XSelectInput(m_dpy, rootwin, SubstructureNotifyMask | PropertyChangeMask);

    XEvent event;
    forever {
        XNextEvent(m_dpy, &event);
        switch(event.type) {
        case ReparentNotify: {
            m_lock.lock();
            if (removeWindow(event.xreparent.window))
                emit windowLost(event.xreparent.window);
            m_lock.unlock();
        }
        case MapNotify: {
            Window wid;
            if (event.type == ReparentNotify) {
                wid = event.xreparent.parent;
            }
            else {
                // qDebug() << "Map: " << QString::number(event.xmap.window, 16);
                wid = event.xmap.window;
            }
            m_lock.lock();

            auto top = XHelper::GetWindowTopAncestor(m_dpy, wid);
            if (top) {
                pid_t pid = XHelper::FindWindowPid(m_dpy, top);
                if (pid != 0) {
                    auto it = m_pidToWindowSet.find(pid);
                    if (it != m_pidToWindowSet.end()) {
                        WindowInfo wi(pid, false);
                        if (addWindowIfNotExist(top, wi)) {
                            emit windowFound(top, pid);
                            QRect rect;
                            XHelper::GetWindowRect(m_dpy, top, rect);
                            emit windowResized(top, QSize(rect.width(), rect.height()));
                        }
                    }
                }
            }
            m_lock.unlock();
			break;
        }
        case UnmapNotify: {
            // qDebug() << "Unmap: 0x" << QString::number(event.xunmap.window, 16);
            m_lock.lock();
            auto it = m_windowToPid.find(event.xunmap.window);
            if (it != m_windowToPid.end()) {
                m_pidToWindowSet[it->m_pid].remove(it.key());
                m_windowToPid.erase(it);
                emit windowLost(event.xunmap.window);
            }
            m_lock.unlock();
            break;
        }
        case ConfigureNotify: {
            m_lock.lock();
            auto it = m_windowToPid.find(event.xconfigure.window);
            if (it != m_windowToPid.end()) {
                emit windowResized(it.key(), QSize(event.xconfigure.width, event.xconfigure.height));
            }
            m_lock.unlock();
            break;
        }
        }
    }
}

void XNotifyThread::attachPid(pid_t pid)
{
    m_lock.lock();
    auto it = m_pidToWindowSet.find(pid);
    if (it == m_pidToWindowSet.end()) {
        m_pidToWindowSet.insert(pid, QSet<WId>());
        m_lock.unlock();
        ForceSyncAllWindow();
    } else {
        m_lock.unlock();
    }
}

void XNotifyThread::detachPid(pid_t pid)
{
    m_lock.lock();
    auto it = m_pidToWindowSet.find(pid);
    if (it != m_pidToWindowSet.end()) {
        auto &windowSet = it.value();
        for (auto w : windowSet) {
            emit windowLost(w);
            m_windowToPid.remove(w);
        }
        m_pidToWindowSet.erase(it);
    }
    m_lock.unlock();
}

#include "mainwindow.h"

#include <QApplication>
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#include <QLoggingCategory>
#endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    QLoggingCategory::defaultCategory()->setEnabled(QtDebugMsg, true);
#endif
    w.show();
    return a.exec();
}

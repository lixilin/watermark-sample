QT       += core gui
LIBS     += -lX11 -lXext -lXmu
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets x11extras

CONFIG += c++11 console
QMAKE_CXXFLAGS += '-std=c++0x'

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x040000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    mark.cpp \
    watermarkmanager.cpp \
    xhelper.cpp \
    xnotifythread.cpp

HEADERS += \
    mainwindow.h \
    mark.h \
    watermarkmanager.h \
    xhelper.h \
    xnotifythread.h

FORMS += \
    mainwindow.ui \
    mark.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# 水印测试程序

# 能力
支持屏幕文本水印、屏幕图像水印、进程文本水印

# 编译

该工程支持Qt4.7或更高版本。

```
$ cd watermark-sample
$ qmake
$ make
$ ./watermark-sample
```

# 界面截图

![Screenshot](./screenshot.png)

# 操作

1. 启用屏幕文本水印：填写 “For text” 信息，填写 “Text” 水印文本，修改“Mode”为“FullScreen”，修改“Style”为“Text”，单击“Apply”，单击“Show”。
2. 启用屏幕图片水印：填写 “For image” 信息，修改“Mode”为“FullScreen”，修改“Style”为“Image”，单击“Apply”，单击“Show”。
3. 启用进程文本水印：填写 ”For text“ 信息，单击 “Refresh“ 刷新窗口列表，选择需要绑定水印的窗口，单击“Add” 将PID添加进 “PIDs” 列表，“Mode” 修改为 ”Process“，“Style” 修改为 “Text”， 单击 “Apply”，单机 “Show”。

# BUGs

当前实现不支持使用Wine模拟的PE应用程序。

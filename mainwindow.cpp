#include "mainwindow.h"
#include "watermarkmanager.h"
#include "xhelper.h"
#include <QDebug>
#include <QDesktopWidget>
#include <QApplication>
#include <QFileDialog>
#include <QHeaderView>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xmu/Atoms.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->twWindows->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->twWindows->verticalHeader()->setHidden(false);
    // ui->twWindows->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
    ui->twWindows->setColumnWidth(0, 300);
    ui->twWindows->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    ui->twWindows->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    ui->twWindows->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->twWindows->setSelectionBehavior(QTableWidget::SelectRows);
    ui->twWindows->setStyleSheet("selection-background-color: grey");

    on_pbRefresh_clicked();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
    qApp->exit();
}

void MainWindow::on_toolButton_clicked()
{
    QFileDialog dlg;
    if (dlg.exec() != QDialog::Accepted)
        return;
    ui->leFilename->setText(dlg.selectedFiles().at(0));
}


void MainWindow::on_cbScale_activated(int)
{

}

void MainWindow::on_pbRefresh_clicked()
{
    Display *dpy = XOpenDisplay(NULL);
    QMap<WId, XHelper::WindowInfo> snapshot;
    XHelper::CreateWindowSnapshot(dpy, snapshot);
    ui->twWindows->setRowCount(snapshot.size());
    int i = 0;
    for (auto it : snapshot) {
        size_t size;
        char *text = XHelper::GetWindowProperty(dpy, it.m_wid, XA_UTF8_STRING(dpy), "_NET_WM_NAME", &size);
        if (text) {
            ui->twWindows->setVerticalHeaderItem(i, new QTableWidgetItem(QString::number(i)));
            ui->twWindows->setItem(i, 0, new QTableWidgetItem(text));
            ui->twWindows->setItem(i, 1, new QTableWidgetItem("0x" + QString::number(it.m_wid, 16)));
            ui->twWindows->setItem(i, 2, new QTableWidgetItem(QString::number(it.m_pid)));
            free(text);
        }
        i++;
    }
}

void MainWindow::on_pbAdd_clicked()
{
    auto items = ui->twWindows->selectedItems();

    if (items.count() == 0)
        return;
    for (int i = 0; i < ui->lwPID->count(); i++) {
        if (ui->lwPID->item(i)->text() == items.at(2)->text())
            return;
    }
    ui->lwPID->addItem(items.at(2)->text());

    WatermarkManager *manager = WatermarkManager::get_instance();
    QColor color;
    switch (ui->cbColor->currentIndex()) {
        case 0: color = Qt::red; break;
        case 1: color = Qt::green; break;
        case 2: color = Qt::blue; break;
    }
    if (ui->cbStyle->currentIndex() == 0) {
        manager->setMark(color,
                        ui->fontComboBox->currentText(),
                        ui->sbFontSize->value(),
                        ui->sbHSpace->value(),
                        ui->sbVSpace->value(),
                        ui->sbAlpha->value(),
                        ui->sbAngle->value());
    } else {
        manager->setMark(ui->leFilename->text(), ui->sbAlpha_2->value(), ui->cbScale->currentIndex());
    }

    manager->bindPID(items.at(2)->text().toInt(), ui->leText->text());


}

void MainWindow::on_pbRemove_clicked()
{
    WatermarkManager *manager = WatermarkManager::get_instance();
    auto item = ui->lwPID->currentItem();
    if (!item)
        return;
    manager->unbindPID(item->text().toInt());
    ui->lwPID->removeItemWidget(item);
    delete item;
}

void MainWindow::on_pbShow_clicked(bool checked)
{
    WatermarkManager *manager = WatermarkManager::get_instance();
    if (checked)
        manager->show();
    else
        manager->hide();
}

void MainWindow::on_cbMode_currentIndexChanged(int index)
{
    WatermarkManager *manager = WatermarkManager::get_instance();
    switch (index) {
        case 0: manager->setMode(WatermarkManager::FullScreen); break;
        case 1: manager->setMode(WatermarkManager::ProcessWindow); break;
    }
}

void MainWindow::on_cbStyle_currentIndexChanged(int index)
{
    // emit ui->pbApply->click();
    bool enable = index == 0;
    ui->gbText->setEnabled(enable);
    ui->gbImage->setEnabled(!enable);
}

void MainWindow::on_pbApply_clicked()
{
    WatermarkManager *manager = WatermarkManager::get_instance();
    QColor color;
    switch (ui->cbColor->currentIndex()) {
        case 0: color = Qt::red; break;
        case 1: color = Qt::green; break;
        case 2: color = Qt::blue; break;
    }

    if (ui->cbMode->currentIndex() == 0) {
        manager->setFullScreenModeText(ui->leText->text());
    }

    if (ui->cbStyle->currentIndex() == 0) {
        manager->setMark(color,
                        ui->fontComboBox->currentText(),
                        ui->sbFontSize->value(),
                        ui->sbHSpace->value(),
                        ui->sbVSpace->value(),
                        ui->sbAlpha->value(),
                        ui->sbAngle->value());
    } else {
        manager->setMark(ui->leFilename->text(), ui->sbAlpha_2->value(), ui->cbScale->currentIndex());
    }
}

#include "watermarkmanager.h"
#include <QDebug>
#include <QApplication>
#include <QDesktopWidget>
#include <QPair>
#include <X11/Xlib.h>
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QX11Info>
#else
#include <QtX11Extras/QX11Info>
#endif
WatermarkManager *WatermarkManager::m_instance = nullptr;

WatermarkManager::WatermarkManager()
{
    m_markWindow.move(0, 0);
    connect(qApp->desktop(), SIGNAL(resized(int)), this, SLOT(desktopResized(int)));
    connect(qApp->desktop(), SIGNAL(screenCountChanged(int)), this, SLOT(screenCountChanged(int)));
    connect(qApp->desktop(), SIGNAL(primaryScreenChanged()), this, SLOT(primaryScreenChanged()));
    m_notifyThread.start();
    m_style = UnsetStyle;
    qRegisterMetaType<pid_t>("pid_t");
    qRegisterMetaType<WId>("WId");

    connect(&m_notifyThread, SIGNAL(windowFound(WId, pid_t)),
            this, SLOT(windowFound(WId, pid_t)), Qt::QueuedConnection);
    connect(&m_notifyThread, SIGNAL(windowLost(WId)),
            this, SLOT(windowLost(WId)), Qt::QueuedConnection);
    connect(&m_notifyThread, SIGNAL(windowFocusIn(WId)),
            this, SLOT(windowFocusIn(WId)), Qt::QueuedConnection);
    connect(&m_notifyThread, SIGNAL(windowFocusOut(WId)),
            this, SLOT(windowFocusOut(WId)), Qt::QueuedConnection);
    connect(&m_notifyThread, SIGNAL(windowResized(WId, QSize)),
            this, SLOT(windowResized(WId, QSize)), Qt::QueuedConnection);

    update();
}

void WatermarkManager::desktopResized(int n)
{
    Q_UNUSED(n);
    int prim = qApp->desktop()->primaryScreen();
    QRect rect = qApp->desktop()->screenGeometry(prim);
    m_markWindow.resize(rect.width(), rect.height());

    if (m_style == Text) {
        m_markWindow.setMark(m_textMark.color, m_textMark.font,
                             m_textMark.fontSize, m_textMark.hspace, m_textMark.vspace,
                             m_textMark.alpha, m_textMark.angle);
    } else if (m_style == Image) {
        m_markWindow.setMark(m_imageMark.image, m_imageMark.alpha, m_imageMark.scale);
    }
}

void WatermarkManager::screenCountChanged(int)
{
    // TODO
}

void WatermarkManager::primaryScreenChanged()
{
    // TODO
}

void WatermarkManager::show()
{
    if (m_visible)
        return;
    m_visible = true;

    if (m_mode == FullScreen) {
        WatermarkManager::desktopResized(0);
        m_markWindow.show();
    } else if (m_mode == ProcessWindow) {
        for (auto mark : m_windowToMarkInfo) {
            mark->m_mark.show();
        }
    }
}

void WatermarkManager::hide()
{
    if (!m_visible)
        return;
    m_visible = false;

    if (m_mode == FullScreen) {
        m_markWindow.hide();
    } else if (m_mode == ProcessWindow) {
        for (auto mark : m_windowToMarkInfo) {
            mark->m_mark.hide();
        }
    }
}

void WatermarkManager::bindPID(pid_t pid, QString text)
{
    auto it = m_pidToText.find(pid);
    if (it != m_pidToText.end()) {
        m_pidToText[pid] = text;
        return;
    }

    m_pidToText.insert(pid, text);
    m_notifyThread.attachPid(pid);

}

void WatermarkManager::unbindPID(pid_t pid)
{
    m_notifyThread.detachPid(pid);
}

void WatermarkManager::setMode(Mode mode)
{
    if (mode == m_mode)
        return;
    m_mode = mode;
    update();
}

void WatermarkManager::updateAllMark()
{
    using MarkInfo = QPair<Mark*, QString>;
    QList<MarkInfo> list;
    if (m_mode == FullScreen) {
        list.append(MarkInfo(&m_markWindow, m_markText));
    } else if (m_mode == ProcessWindow) {
        for (auto mark : m_windowToMarkInfo) {
            list.append(MarkInfo(&m_markWindow, m_pidToText[mark->m_pid]));
        }
    }
    for (auto mark : list) {
        mark.first->setText(mark.second);
        if (m_style == Text) {
            mark.first->setMark(m_textMark.color, m_textMark.font,
                                 m_textMark.fontSize, m_textMark.hspace, m_textMark.vspace,
                                 m_textMark.alpha, m_textMark.angle);
        } else if (m_style == Image) {
            mark.first->setMark(m_imageMark.image, m_imageMark.alpha, m_imageMark.scale);
        }
    }
}

void WatermarkManager::updateVisibility()
{
    bool processMarkVisible = false;
    bool screenMarkVisible = false;

    if (m_visible) {
        if (m_mode == FullScreen) {
            screenMarkVisible = true;
        } else if (m_mode == ProcessWindow) {
            processMarkVisible = true;
        }
    }

    m_markWindow.setVisible(screenMarkVisible);
    for (auto mark : m_windowToMarkInfo) {
        mark->m_mark.setVisible(processMarkVisible);
    }
}

void WatermarkManager::update()
{
    updateVisibility();
    updateAllMark();
}

void WatermarkManager::windowFound(WId w, pid_t pid)
{
    qDebug() << "WatermarkManager::windowFound(" << QString::number(w, 16) << "," << pid << ")";

    auto it = m_pidToText.find(pid);
    if (it == m_pidToText.end()) {
        qDebug() << "Error: pid is not registered unexpectly";
        return;
    }

    MarkInfo *mark = new MarkInfo();
    mark->m_pid = pid;

    m_windowToMarkInfo.insert(w, mark);
    mark->m_mark.setStayOnTop(true);
    mark->m_mark.setText(*it);
    if (m_style == Text) {
        mark->m_mark.setMark(m_textMark.color, m_textMark.font,
                             m_textMark.fontSize, m_textMark.hspace, m_textMark.vspace,
                             m_textMark.alpha, m_textMark.angle);
    } else if (m_style == Image) {
        mark->m_mark.setMark(m_imageMark.image, m_imageMark.alpha, m_imageMark.scale);
    }

    Display *dpy = QX11Info::display();
    if (False == XReparentWindow(dpy, mark->m_mark.winId(), w, 0, 0)) {
        delete mark;
        return;
    }

    XRaiseWindow(dpy, mark->m_mark.winId());

    if (m_mode != ProcessWindow) {
        mark->m_mark.hide();
    } else
        mark->m_mark.setVisible(m_visible);
}

void WatermarkManager::windowLost(WId w)
{
    qDebug() << "WatermarkManager::windowLost(" << QString::number(w, 16) << ")";
    auto it = m_windowToMarkInfo.find(w);
    if (it != m_windowToMarkInfo.end()) {
        delete it.value();
        m_windowToMarkInfo.erase(it);
    }
}

void WatermarkManager::windowResized(WId w, QSize size)
{
    qDebug() << "WatermarkManager::windowResized(" << QString::number(w, 16) << "," << size << ")";
    auto it = m_windowToMarkInfo.find(w);
    if (it != m_windowToMarkInfo.end()) {
        auto *mark = &it.value()->m_mark;
        bool v = mark->isVisible();
        mark->show();
        mark->resize(size.width(), size.height());
        mark->move(0, 0); // For Kylin, mark will move out of the window when resizing.
        mark->setVisible(v);

        if (m_style == Text) {
            mark->setMark(m_textMark.color, m_textMark.font,
                                 m_textMark.fontSize, m_textMark.hspace, m_textMark.vspace,
                                 m_textMark.alpha, m_textMark.angle);
        } else if (m_style == Image) {
            mark->setMark(m_imageMark.image, m_imageMark.alpha, m_imageMark.scale);
        }
        mark->raise();
    }
}

void WatermarkManager::windowFocusIn(WId w)
{
    //qDebug() << "WatermarkManager::windowFocusIn(" << w << ")";
    auto it = m_windowToMarkInfo.find(w);
    if (it != m_windowToMarkInfo.end()) {
        XRaiseWindow(QX11Info::display(), it.value()->m_mark.winId());
    }
}

void WatermarkManager::windowFocusOut(WId w)
{
    //qDebug() << "WatermarkManager::windowFocusOut(" << w << ")";
    auto it = m_windowToMarkInfo.find(w);
    if (it != m_windowToMarkInfo.end()) {
    }
}
